#KD8RHO Utility Library

This is the KD8RHO utility library, designed to make commonly used methods more simple to use


Current Version: 1.9.1

####Changelog:
1.9.2 Added `getInnerDirectoryName()` to `Utility` class.

1.9.2 Made `Utility` class methods static

1.9.1 Made `TextMenu` chainable. Added `Utility` class with small number of non-class-based convenience methods.

1.9.0 Added `CommandLineArgumentParser` and `CommandLineArgument` classes for dealing with command line arguments. Made SJIO method-chainable.

1.8.2 Fixed errors with get*() from `SJIO` causing the input to wait for an extra line...

1.8.1 `Web` class now stores last HTTP error it got. This allows the error to be retrieved by any application using the library

1.8.0
Added `PairedList` ([docs](https://docs.kd8rho.net/net/kd8rho/util/data/PairedList.html)) class
This class stores a pair of `ArrayList`s, allowing you to compare two lists.

1.7.5
Changed capitalization of `Web` class. This will break old code.
Changed `Web` output.
Added `getPreference` to Preferences.java.
Annotation changes to Preferences.java.
Forced `SJIO` to use system line separator when printing out.


1.7.4
Add `Name` class to data

1.7.3
Minor bugfix for missing else in [Preference](https://docs.kd8rho.net/net/kd8rho/util/Preference.html)

1.7.2
Fix bug with `getInt` in [SJIO](https://docs.kd8rho.net/net/kd8rho/util/SJIO.html) not behaving properly.
Code cleanup

1.7.1
Fix bug with `Preference` not writing as it should.
Improved `Preference` JavaDocs and Annotations

1.7
Added [Preferences](https://docs.kd8rho.net/net/kd8rho/util/Preferences.html) class to store and deal with user preferences

1.6.4:
Added [TextMenu.findOption()](https://docs.kd8rho.net/net/kd8rho/util/TextMenu.html#findOption(java.lang.String)) to find an option

1.6.3-41:
Fixed `NoClassDefError` when initializing [SJIO](https://docs.kd8rho.net/net/kd8rho/util/SJIO.html) on Android devices (or contexts without the VM Manager class)

1.6.3:

Added auto-log ability to `SJIO`. Also added ability to set debug mode (for uses where debug mode may not work such as Android)

Added contingency for escape characters in the log

Forcing `writetolog` and `location` static

1.6.2

Added `writeToLogFile`. Added ability to print output to log file

1.6.1
Allowed ability to set [SJFIO](https://docs.kd8rho.net/net/kd8rho/util/SJFIO.html) charset. Default is UTF-8.


Assorted cleanup for Credential.java

Assorted cleanup for SJFIO.java

Assorted cleanup for SJIO.java

Assorted cleanup for TextMenu.java

Assorted cleanup for web.java

Added ability to get logfile location from `SJIO`

Added [Credential](https://docs.kd8rho.net/net/kd8rho/util/Credential.html) and [NotYetImplementedException](https://docs.kd8rho.net/net/kd8rho/util/NotYetImplementedException.html) classes, various other changes to SJIO


##Contents:
###SJIO:
[SJIO (SJ Input/Output)](https://docs.kd8rho.net/net/kd8rho/util/SJIO.html) is a library which wraps `system.in` and `system.out`
It also implements error checking and input validation
Example usage:

`SJIO io=new SJIO();`

`io.writeLine("This writes a String");`

`int input=io.getInt();//This will get an int without any kind of prompt.`

`int input2=io.promptInt("Please input an int","Please input a valid int");//This will ask for an int, displaying the second string if a valid int is not found`

`int input3=io.promptInt("Please input an int between 2 and 3","Please input a valid int between 2 and 3",2,3)//This will ask for an int and verify it's between 2 and 3`

`int input4=io.promptInt("Please input an intbetween 1 and 5, or input -1 to exit","Please input a valid response",">0&<6|=-1")//This will ask for an int and verify it using a format string`

#### Format Strings:
Format Strings are number verifications.
There are 4 possible operators:

`>`:Greater than

`<`:Less than

`=`:Equal to

`!`:Not equal to

Formats can be chained using `&` for `and` and `|` for `or`
A complex format string can be built using these directives, for example:

`">0&<"+theOptions.size+"|"+DEVMENU_OPTION+"|"+EXIT;`




###TextMenu:
TextMenu is a text-based menu builder designed to build "main menus" and other text-based menus.
Input validation is done automatically, allowing for very simple code.

Example Usage:
`TextMenu menu=new TextMenu("Main Menu",new String[] "Hello world","Exit");`

`menu.displayMenu();`

would display:

`Main Menu`

`1. Hello World`

`2. Exit`

`>`

###Web:
The Web library is a library designed to allow for easy reading of webpages into text files.

The web library will handle exceptions and return only an error message or valid data to the user. It can also use http authentication to log in prior to downloading.

Usage:

`
web.getURLTextToArray("https://www.website.com/textfile.txt");
`

returns the contents of textfile.txt as an `ArrayList<String>`

