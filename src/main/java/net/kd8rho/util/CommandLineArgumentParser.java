/*
 * Copyright 2014 Steven Jennison https://kd8rho.net, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * Created by Steven on 9/7/2014. Package: net.kd8rho.util Project: Util
 *
 * Class to parse standard command line arguments
 */
@SuppressWarnings("UnusedDeclaration")
/*Library class, unused declaration is irrelevant*/
public class CommandLineArgumentParser {

    /**
     * Gets the description of the program
     * @return the description
     */
    public String getTheDescription() {
        return theDescription;
    }

    /**
     * Sets the description of the program
     * @param theDescription the description of the program
     */
    public void setTheDescription(String theDescription) {
        this.theDescription = theDescription;
    }

    private String theDescription;
    private ArrayList<CommandLineArgument> theArguments=new ArrayList<>();

    /**
     * Checks if the given arg is a switch
     * @param arg arg to check
     * @return true if it is a switch
     */
    private boolean checkIfSwitch(@NotNull String arg)
    {
        char first;
        try{
            first = arg.charAt(0);
        }
        catch (Exception e)
        {
            return false;
        }
        return first == '-' || first == '/';
    }
    /**
     * Instantiates new CommandLineArgumentParser with the given args as a parameter
     * @param theArguments the arguments to use for parsing
     */
    public CommandLineArgumentParser(@NotNull ArrayList<CommandLineArgument> theArguments) {
        this.theArguments=theArguments;
    }

    /**
     *
     * @param args the arguments to parse
     * @throws UnrecognizedArgumentException
     */
    public void parse(@NotNull String[] args) throws UnrecognizedArgumentException {
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            CommandLineArgument theCurrentArg;
            if(checkIfSwitch(arg)) {
                int current=i;
                theCurrentArg = parseArg(arg);
                arg = args[i+1];
                while (!checkIfSwitch(arg)&&i+1<args.length) {
                    i++;
                    this.theArguments.remove(current);
                    theCurrentArg.addOption(arg);
                    if(i+1<args.length)
                        arg = args[i+1];
                }
                theCurrentArg.setActive(true);
                this.theArguments.add(theCurrentArg);
            }

        }
    }

    /**
     * Parses an argument
     * @param arg argument to parse
     * @return the CommandLineArgument
     * @throws UnrecognizedArgumentException if the argument isn't found
     */
    @NotNull
    private CommandLineArgument parseArg(@NotNull String arg) throws UnrecognizedArgumentException {
        arg=arg.replaceAll("-","").replaceAll("/","");
        for (CommandLineArgument theArgument : theArguments) {
            for (String argument : theArgument.getTheArguments()) {
                if(arg.equals(argument))
                    return theArgument;
            }
        }
        throw new UnrecognizedArgumentException(arg);
    }

    /**
     * Prints the usage to the stdout buffer
     */
    public void printUsage()
    {
        SJIO io=new SJIO();
        io.writeLine(theDescription);
        io.writeLine("Usage: ");
        for (CommandLineArgument theArgument : theArguments) {
            ArrayList<String> theArguments1 = theArgument.getTheArguments();
            for (int i = 0; i < theArguments1.size(); i++) {
                String arg = theArguments1.get(i);
                io.write((i>0?", -":"-")+arg);
            }
            io.write(": ");
            io.writeLine(theArgument.getTheDescription());
        }
    }

    /**
     * Gets an argument by name
     * @param argName the name
     * @return the argument. Null if not found.
     */
    public CommandLineArgument getArg(@NotNull String argName)
    {
        for (CommandLineArgument theArgument : theArguments) {
            for (String arg : theArgument.getTheArguments()) {
                if(argName.equals(arg))
                    return theArgument;
            }
        }
        return null;
    }
}
