/*
 * Copyright 2014 Steven Jennison https://kd8rho.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Class to write to and read from a file.
 *
 * Created by Steven on 3/6/14. Package: net.kd8rho.util Project: School-14SP <p/>
 * <p/>
 * Version 1.1 <p/> Changelog: <p/> 1.0: Initial version <p/> 1.1: Added NoSuchFileException to catch statement
 */
@SuppressWarnings("UnusedDeclaration")
public class SJFIO {
    @SuppressWarnings("CanBeFinal")
    private static SJIO io = new SJIO();
    /**
     * The ArrayList that stores the data from the file
     */
    @SuppressWarnings("WeakerAccess")
    public ArrayList<String> theFileInMem = new ArrayList<>();
    private Charset currentCharset = StandardCharsets.UTF_8;
    private String filePath;

    /**
     * Constructor
     *
     * @param filePath the file to read from && write to
     */
    public SJFIO(String filePath) {
        this.filePath = filePath;
    }

    public Charset getCurrentCharset() {
        return currentCharset;
    }

    public void setCurrentCharset(Charset currentCharset) {
        this.currentCharset = currentCharset;
    }

    /**
     * Gets path of file
     *
     * @return current filepath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Changes filepath
     *
     * @param filePath new filepath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Read file to theFileInMem, not throwing FileNotFoundException if it's not found
     *
     * @throws IOException if an exception occurs while reading
     */
    public void read() throws IOException {
        read(false);
    }

    /**
     * Read file to theFileInMem
     *
     * @param throwFileNotFound whether to throw a FileNotFoundException when the file isn't found
     * @throws IOException if an exception occurs while reading
     */
    @SuppressWarnings({"SameParameterValue", "WeakerAccess"})
    public void read(boolean throwFileNotFound) throws IOException {
        try {
            theFileInMem = new ArrayList<>(Files.readAllLines(Paths.get(filePath), currentCharset));
        } catch (FileNotFoundException | NoSuchFileException ex) {
            //File not found...
            //This is OK, we just make it now...
            io.log("SJFIO", "read", "File at " + filePath + " not found. This is not necessarily an error!", SJIO.LEVEL.INFO);
            if (throwFileNotFound)
                throw ex;
        } catch (IOException ex) {
            io.log("SJFIO", "read", ex.toString() + " while reading file.\nStack trace: " + ex.getMessage(), SJIO.LEVEL.WARN);
            throw ex;
        }
    }

    /**
     * Writes theFileInMem to the file.
     *
     * @throws IOException
     */
    public void write() throws IOException {
        try {
            Files.write(Paths.get(filePath), theFileInMem, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            io.log("SJFIO", "write", ex.toString() + " while writing file.\nStack trace: " + ex.getMessage(), SJIO.LEVEL.WARN);
            throw ex;
        }
    }
}
