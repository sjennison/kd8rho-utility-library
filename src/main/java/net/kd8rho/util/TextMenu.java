/*
 * Copyright 2014 Steven Jennison https://kd8rho.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This is a text menu that can be used to display options and get user input. Designed to be highly modular.
 *
 * Created by Steven on 2/24/14. Package: net.kd8rho.util Project: Util
 * <p/>
 *
 * @author Steven Jennison
 * @version 1.2
 */
public class TextMenu {
    private ArrayList<String> menuOptions = new ArrayList<>();
    private String seperator = "\t";
    private String prompt = null;
    private String errorPrompt = "Please input a valid number";
    private boolean isReady;
    private SJIO io = new SJIO();
    private String format = null;

    /**
     * Default constructor. Things need to be initialized before it can be used!
     */
    public TextMenu() {
        isReady = false;
    }

    /**
     * Constructor with options and prompt. Ready to use after this!
     *
     * @param prompt  The prompt to display to the user
     * @param options A String array of options
     */
    public TextMenu(String prompt, String[] options) {
        isReady = true;
        this.prompt = prompt;
        this.menuOptions = new ArrayList<>(Arrays.asList(options));
    }

    /**
     * Constructor with options and prompt. Ready to use after this!
     *
     * @param prompt  The prompt to display to the user
     * @param options A String ArrayList of options
     */
    public TextMenu(String prompt, ArrayList<String> options) {
        isReady = true;
        this.prompt = prompt;
        this.menuOptions = options;
    }

    /**
     * Constructor with just a prompt - need to add options too
     *
     * @param prompt The prompt to display to the user
     */
    public TextMenu(String prompt) {
        isReady = false;
        this.prompt = prompt;
    }

    /**
     * Gets the seperator to use when displaying the menu
     *
     * @return seperator
     */
    public String getSeperator() {
        return seperator;
    }

    /**
     * Sets the seperator to use when displaying the menu
     *
     * @param seperator the seperator to use
     */
    public TextMenu setSeperator(String seperator) {
        this.seperator = seperator;
        return this;
    }

    /**
     * Gets the prompt to use in the menu
     *
     * @return prompt
     */
    public String getPrompt() {
        return prompt;
    }

    /**
     * Sets the prompt to use in the menu
     *
     * @param prompt the prompt to use
     */
    public TextMenu setPrompt(String prompt) {
        this.prompt = prompt;
        testReady();
        return this;
    }

    /**
     * Returns the maximum valid choice.
     *
     * @return the maximum choice a user can put in. Ignores special format
     */
    public int getMax() {
        return this.menuOptions.size();
    }

    /**
     * Sets the error prompt to display on bad user input.
     *
     * @param errorPrompt Error prompt to show the user
     */
    public TextMenu setErrorPrompt(String errorPrompt) {
        this.errorPrompt = errorPrompt;
        return this;
    }

    /**
     * Sets an extra valid format to be used for user input (for things such as secret functions...)
     *
     * @param format the format string to use
     */
    public TextMenu setFormat(String format) {
        this.format = format;
        return this;
    }

    /**
     * Tests if the menu has enough data to start. If not, it will not show the menu.
     */
    private void testReady() {
        isReady = (prompt != null && menuOptions.size() > 0);
    }

    /**
     * Tests if the menu is ready.
     *
     * @return true if menu can be displayed, false if it cannot
     */
    public boolean isReady() {
        return isReady;
    }

    /**
     * Adds an option to the list
     *
     * @param option The option to add
     */
    public TextMenu addOption(String option) {
        this.menuOptions.add(option);
        testReady();
        return this;
    }

    /**
     * Removes an option. Returns false if option invalid.
     *
     * @param optionIndex The option to remove, 0 based
     * @return whether the operation completed successfully
     */
    public boolean removeOption(int optionIndex) {
        try {
            this.menuOptions.remove(optionIndex);
            return true;
        } catch (ArrayIndexOutOfBoundsException ex) {
            io.log("net.kd8rho.util.TextMenu", "removeOption", "Cannot remove option - index is out of bounds!", SJIO.LEVEL.ERROR);
            return false;
        }
    }

    /**
     * The real workhorse of the class, this method displays the menu and gets user input.
     *
     * @return The user's response
     */
    public int displayMenu() {
        return displayMenu(">");
    }

    /**
     * The real workhorse of the class, this method displays the menu and gets user input.
     *
     * @param prompt The prompt to display at the end of the menu (default is ">")
     * @return The user's response
     */
    public int displayMenu(String prompt) {
        if (isReady) {
            io.writeLine(this.prompt);
            int currentOption = 1;
            for (String option : this.menuOptions) {
                io.writeLine(currentOption + "." + seperator + option);
                currentOption++;
            }
            return io.promptInt(prompt, errorPrompt, (this.format == null ? ">0&<" + currentOption : ">0&<" + currentOption + "|" + this.format));
        } else {
            io.log("net.kd8rho.util.TextMenu", "displayMenu", "Menu not initialized! This is a problem!", SJIO.LEVEL.ERROR);
            return (-1);
        }
    }

    /**
     * Finds the index of the given option. Returns -1 if not found.
     *
     * @param option the option to search for
     * @return the index of the option, -1 if not found.
     */

    public int findOption(String option) {
        for (int i = 0; i < menuOptions.size(); i++) {
            String menuOption = menuOptions.get(i);
            if (menuOption.equals(option))
                return i + 1;
        }
        return -1;
    }


}
