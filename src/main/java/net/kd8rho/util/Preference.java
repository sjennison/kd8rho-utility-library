/*
 * Copyright 2014 Steven Jennison https://kd8rho.net, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A POJO to store a single Preference with a string value
 *
 * Created by Steven on 6/26/2014. Package: net.kd8rho.util Project: Util
 */

@SuppressWarnings("UnusedDeclaration")
public class Preference {
    @NotNull
    public String name;
    @Nullable
    public String value;

    /**
     * Constructor that parses a given string into a Preference
     * @param prefString String to parse
     */
    public Preference(@NotNull String prefString)
    {
        if(prefString.charAt(0)=='#'||prefString.equals(""))//This is a comment or an empty line. Keep this line to maintain file structure.
            name=prefString;
        else {
            try {
                String[] splitString = prefString.split(": ");
                name = splitString[0];
                value = splitString[1];
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Invalid preference line: \'" + prefString + "\' received.");
            }
        }
    }

    /**
     * Constructor that builds a Preference out of a given name and value/
     * @param name name of the Preference
     * @param value value of the Preference
     */
    public Preference(@NotNull String name, @Nullable String value)
    {
        this.name=name;
        this.value=value;
    }

    /**
     * Gets the name
     * @return the name of the Preference
     */
    @NotNull
    public String getName() {
        return name;
    }

    /**
     * Sets the name
     * @param name the name of the Preference
     */
    public void setName(@NotNull String name) {
        this.name = name;
    }

    @Nullable
    public String getValue() {
        return value;
    }

    public void setValue(@Nullable String value) {
        this.value = value;
    }

    @NotNull
    public String toLine()
    {
        return(name+(value!=null?": "+value:""));
    }
}
