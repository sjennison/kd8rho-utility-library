/*
 * Copyright 2014 Steven Jennison https://kd8rho.net, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Preference manager. Stores preferences and has ability to write to and read from files.
 * <p/>
 * Created by Steven on 6/26/2014. Package: net.kd8rho.util Project: Util
 */
@SuppressWarnings("UnusedDeclaration")
public class Preferences {
    @NotNull
    private ArrayList<Preference> thePreferences = new ArrayList<>();
    @NotNull
    private String settingFileLocation;


    /**
     * Default constructor, reads from "settings.cfg"
     *
     * @throws IOException
     */
    public Preferences() throws IOException {
        settingFileLocation = "settings.cfg";
        loadFromFile();
    }

    /**
     * Constructor that takes a String as a filename to read from and write to
     *
     * @param theSettingFile the location the settings file is being saved to
     * @throws IOException
     */
    public Preferences(@NotNull String theSettingFile) throws IOException {
        settingFileLocation = theSettingFile;
        loadFromFile();
    }

    /**
     * Constructor that takes an ArrayList of Preferences to preload data with
     *
     * @param thePreferences an ArrayList of Preferences to preload data with
     */
    public Preferences(@NotNull ArrayList<Preference> thePreferences) {
        settingFileLocation = "settings.cfg";
        this.thePreferences = thePreferences;
    }

    /**
     * Constructor that takes a location for a settings file and an ArrayList of Preferences to preload data with
     *
     * @param theSettingsFile the location the settings file is being saved to
     * @param thePreferences  an ArrayList of Preferences to preload data with
     */
    public Preferences(@NotNull String theSettingsFile, @NotNull ArrayList<Preference> thePreferences) {
        settingFileLocation = theSettingsFile;
        this.thePreferences = thePreferences;
    }

    /**
     * Gets the preference with the name equal to the given string.
     * @param preferenceTag The name to search for
     * @return The found preference, null if not found.
     */
    @Nullable
    public Preference getPreference(@NotNull String preferenceTag) {
        if(preferenceTag.charAt(0)=='#'||preferenceTag.equals(""))//Cannot get blank or comment
            return null;
        for (Preference thePreference : thePreferences) {
            if (thePreference.getName().equals(preferenceTag)) {
                return thePreference;//Return found pref
            }
        }
        return null;//Return null if not found
    }

    /**
     * Gets the preference with the name equal to the given string.
     * @param preferenceTag The name to search for
     * @param defaultValue The value to return if not found
     * @return The found preference, null if not found.
     */
    @Nullable
    public Preference getPreference(@NotNull String preferenceTag,Preference defaultValue) {
        if(preferenceTag.charAt(0)=='#'||preferenceTag.equals(""))//Cannot get blank or comment
            return defaultValue;
        for (Preference thePreference : thePreferences) {
            if (thePreference.getName().equals(preferenceTag)) {
                return thePreference;//Return found pref
            }
        }
        return defaultValue;
    }


    /**
     * Get an arraylist of preferences
     *
     * @return the arraylist of preferences
     */
    @NotNull
    public ArrayList<Preference> getThePreferences() {
        return thePreferences;
    }

    /**
     * Set a specific preference. Will overwrite if already exists
     *
     * @param preferenceTag   the preference name to search for
     * @param preferenceValue the value to write
     */
    public void setPreference(@NotNull String preferenceTag, @Nullable String preferenceValue) {
        for (Preference thePreference : thePreferences) {
            if (thePreference.name.equals(preferenceTag)) {
                thePreference.value = preferenceValue;
                return;
            }
        }
        thePreferences.add(new Preference(preferenceTag, preferenceValue));
    }

    /**
     * Set a specific preference. Will overwrite if already exists.
     *
     * @param preference the preference to write
     */
    public void setPreference(@NotNull Preference preference) {
        for (Preference thePreference : thePreferences) {
            if (thePreference.name.equals(preference.getName())) {
                thePreference.value = preference.getValue();
                return;
            }
        }
        thePreferences.add(preference);
    }

    /**
     * Gets the location the settings file is being saved to
     *
     * @return the location the settings file is being saved to
     */
    @NotNull
    public String getSettingFileLocation() {
        return settingFileLocation;
    }

    /**
     * Sets the location the settings file is being saved to
     *
     * @param settingFileLocation the location the settings file is being saved to
     */
    public void setSettingFileLocation(@NotNull String settingFileLocation) {
        this.settingFileLocation = settingFileLocation;
    }

    /**
     * Loads settings from set file. Overwrites existing data
     *
     * @throws IOException
     */
    public void loadFromFile() throws IOException {
        loadFromFile(false);
    }

    /**
     * Loads settings from set file, with the option to not overwrite existing data
     *
     * @param append whether or not to append to existing data
     * @throws IOException
     */
    public void loadFromFile(boolean append) throws IOException {
        loadFromFile(settingFileLocation, append);
    }

    /**
     * Loads settings from given file, overwriting existing data
     *
     * @param theSettingFile file to read from
     * @throws IOException
     */
    public void loadFromFile(@NotNull String theSettingFile) throws IOException {
        loadFromFile(theSettingFile, false);
    }

    /**
     * Loads settings from given file giving the option to overwrite existing data
     *
     * @param theSettingFile file to read from
     * @param append         whether or not to append to existing data
     * @throws IOException
     */
    public void loadFromFile(@NotNull String theSettingFile, boolean append) throws IOException {
        if (!append) {
            thePreferences = new ArrayList<>();
        }
        SJFIO file = new SJFIO(theSettingFile);
        file.read();
        for (String line : file.theFileInMem) {
            this.setPreference(new Preference(line));
        }
    }

    /**
     * Saves settings to the set file
     *
     * @throws IOException
     */
    public void saveToFile() throws IOException {
        saveToFile(false);
    }

    /**
     * Saves settings to the set file, giving the option to append if needed
     *
     * @param append whether or not to append to existing data
     * @throws IOException
     */
    public void saveToFile(boolean append) throws IOException {
        saveToFile(settingFileLocation, false);
    }

    /**
     * Saves settings to the given file
     *
     * @param theSettingFile file to save to
     * @throws IOException
     */
    public void saveToFile(@NotNull String theSettingFile) throws IOException {
        saveToFile(theSettingFile, false);
    }

    /**
     * Saves settings to the set file, giving the option to append if needed
     *
     * @param theSettingFile file to save to
     * @param append         whether or not to append to existing data
     * @throws IOException
     */
    public void saveToFile(@NotNull String theSettingFile, boolean append) throws IOException {
        SJFIO file = new SJFIO(theSettingFile);
        if (append)
            file.read();
        for (Preference thePreference : thePreferences) {
            file.theFileInMem.add(thePreference.toLine());
        }

        file.write();
    }
}
