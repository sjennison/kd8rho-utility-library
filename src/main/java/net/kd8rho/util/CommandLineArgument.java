/*
 * Copyright 2014 Steven Jennison https://kd8rho.net, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

/**
 * Created by Steven on 9/7/2014. Package: net.kd8rho.util Project: Util
 */
@SuppressWarnings("UnusedDeclaration")
/*Library class, unused declaration is irrelevant*/
public class CommandLineArgument {
    ArrayList<String> theArguments;
    String theDescription;
    boolean active;
    ArrayList<String> options=new ArrayList<>();

    /**
     * Instantiates new <code>CommandLineArgument</code> with a single argument/switch
     * @param theArgument the argument
     */
    public CommandLineArgument(@NotNull String theArgument)
    {
        theArguments=new ArrayList<>();
        theArguments.add(theArgument);
    }

    /**
     * Instantiates new <code>CommandLineArgument</code> with a single argument/switch and description
     * @param theArgument the argument
     * @param theDescription the description to add
     */
    public CommandLineArgument(@NotNull String theArgument,@NotNull String theDescription)
    {
        theArguments=new ArrayList<>();
        theArguments.add(theArgument);
        this.theDescription=theDescription;
    }

    /**
     * Instantiates new <code>CommandLineArgument</code> with multiple arguments/switches
     * @param theArguments the arguments to add
     */
    public CommandLineArgument(@NotNull ArrayList<String> theArguments) {
        this.theArguments = theArguments;
    }

    /**
     * Instantiates new <code>CommandLineArgument</code> with multiple arguments/switches and a description
     * @param theArguments the arguments to add
     * @param theDescription the description to add
     */
    public CommandLineArgument(@NotNull ArrayList<String> theArguments,@NotNull String theDescription) {
        this.theArguments = theArguments;
        this.theDescription = theDescription;
    }

    /**
     * Gets the arguments
     * @return the arguments
     */
    @NotNull
    public ArrayList<String> getTheArguments() {
        return theArguments;
    }

    /**
     * Sets the arguments
     * @param theArguments the arguments to set
     * @return a handle to this object
     */
    @NotNull
    public CommandLineArgument setTheArguments(ArrayList<String> theArguments) {
        this.theArguments = theArguments;
        return this;
    }

    /**
     * gets the description
     * @return the description
     */
    @Nullable
    public String getTheDescription() {
        return theDescription;
    }

    /**
     * Sets the description
     * @param theDescription the description to set
     * @return a handle to this object
     */
    @NotNull
    public CommandLineArgument setTheDescription(@NotNull String theDescription) {
        this.theDescription = theDescription;
        return this;
    }

    /**
     * Returns true if the command line argument has been set "active"
     * @return true if active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets if the argument is active
     * @param active boolean to indiate if active
     * @return a handle to this object
     */
    @NotNull
    public CommandLineArgument setActive(boolean active) {
        this.active = active;
        return this;
    }

    /**
     * Gets the options set for this argument
     * @return the options
     */
    @NotNull
    public ArrayList<String> getOptions() {
        return options;
    }

    /**
     * Sets the options for this argument
     * @param options the options
     * @return a handle to this object
     */
    @NotNull
    public CommandLineArgument setOptions(@NotNull ArrayList<String> options) {
        this.options = options;
        return this;
    }

    /**
     * Adds an option
     * @param theOption the option to add
     * @return a handle to this object
     */
    @NotNull
    public CommandLineArgument addOption(@NotNull String theOption)
    {
        this.options.add(theOption);
        return this;
    }

    /**
     * Adds an argument to this object
     * @param arg the argument
     * @return a handle to this object
     */
    @NotNull
    public CommandLineArgument addArgument(@NotNull String arg) {
        this.theArguments.add(arg);
        return this;
    }
}
