/*
 * Copyright 2014 Steven Jennison https://kd8rho.net, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Contains convenience methods.
 * Created by Steven on 9/8/2014. Package: net.kd8rho.util Project: Util
 */
@SuppressWarnings("UnusedDeclaration")
public class Utility {

    /**
     * Concatenates the strings in an Array using the default separator of a single space
     * @param stringCollection the Collection with strings to concatenate
     * @param separator the separator
     * @return the concatenated string
     */
    public static String concatStringArray(@NotNull Collection<String> stringCollection,@NotNull String separator)
    {
        return concatStringArray((String[]) stringCollection.toArray(),separator);
    }

    /**
     * Concatenates the strings in an Array using the default separator of a single space
     * @param stringCollection the Collection with strings to concatenate
     * @return the concatenated string
     */
    public static String concatStringArray(@NotNull Collection<String> stringCollection)
    {
        return concatStringArray((String[]) stringCollection.toArray());
    }

    /**
     * Concatenates the strings in an Array using the default separator of a single space
     * @param theArray the array with strings to concatenate
     * @return the concatenated string
     */
    @NotNull
    public static String concatStringArray(@NotNull String[] theArray) {
        return concatStringArray(theArray, " ");
    }

    /**
     * Concatenates the strings in an Array using the given separator
     * @param theArray the array with strings to concatenate
     * @param separator the separator
     * @return the concatenated string
     */
    @NotNull
    public static String concatStringArray(@NotNull String[] theArray,@NotNull String separator)
    {
        StringBuilder theBuilder=new StringBuilder();
        for (String item : theArray) {
            theBuilder.append(item).append(separator);
        }
        return theBuilder.toString().trim();
    }

    /**
     * Gets inner directory name, by removing everything before a path seperator "/ or \"
     * Should be platform independent, ignores spaces and other cruft.
     * @param fullPath The full path to find a subdirectory of
     * @return the innermost subdirectory
     */
    @NotNull
    public static String getInnerDirectoryName(@NotNull String fullPath)
    {
        return fullPath.replaceAll("(.*)(\\/|\\\\)","");
    }
}
