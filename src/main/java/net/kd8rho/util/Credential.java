package net.kd8rho.util;

import org.jetbrains.annotations.NotNull;

/**
 * Credential storage container. Stores username and password, and provides methodology to decrypt it.
 * Created by Steven on 5/8/14. Package: net.kd8rho.util Project: Util
 */
@SuppressWarnings("UnusedDeclaration")
public class Credential {
    private String username;
    private String password;
    private StorageType storageType;

    public Credential(@NotNull String username, @NotNull String password, @NotNull StorageType storageType) {
        this.username = username;
        this.password = password;
        this.storageType = storageType;
    }

    @NotNull
    public String getUsername() {
        return username;
    }

    public void setUsername(@NotNull String username) {
        this.username = username;
    }

    @NotNull
    public String getPassword() {
        return password;
    }

    public void setPassword(@NotNull String password) {
        this.password = password;
    }

    public String getPlainTextPassword() throws NotYetImplementedException {
        if (storageType == StorageType.plaintext) {
            return password;
        } else {
            throw new NotYetImplementedException(this.getClass(), "getPlainTextPassword", "non-plaintext options are not yet supported!");
        }
    }

    @NotNull
    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(@NotNull StorageType storageType) {
        this.storageType = storageType;
    }

    /**
     * Storage type of credentials - indicates how the credentials are stored.
     */
    public enum StorageType {
        plaintext, b8e, sha, md5, b64
    }

}
