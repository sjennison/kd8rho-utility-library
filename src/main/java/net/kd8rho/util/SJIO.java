/*
 * Copyright 2014 Steven Jennison https://kd8rho.net, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Input-output class to work with STDIO and other IO streams. Also provides rudimentary logging functionality.
 * Created by Steven Jennison on 1/21/14.
 * <p/>
 * Package: net.kd8rho.util
 * <p/>
 * Project: Util
 * <p/>
 * changelog:
 * <p/>
 * 1.1:
 * <p/>
 * Changed all prompts to use write, instead of writeLine
 * <p/>
 * 1.2:
 * <p/>
 * Added getLine, fixed input-skipping resulting from non-clean inputBuffer
 * <p/>
 * 1.3:
 * <p/>
 * * Added defaults, changed behavior of log to only log warns unless in debug mode
 * <p/>
 * or told to force. Overloaded method added to ensure backward-compatibility
 * <p/>
 * * Added enum type to log - deprecated string version
 * <p/>
 * * Added ERROR log level - logs "warn" but appends "E:" to the beginning of the
 * <p/>
 * <p/>
 * 1.4:
 * <p/>
 * * Added boolean to log only to file (to minimize console junk)
 * <p/>
 * * Fixing typos in javadoc changelog (meta-changelog!)
 * <p/>
 * * Added constructor that can take an already-initialized scanner.
 * <p/>
 *
 * @author Steven Jennison
 * @version 1.4
 */
@SuppressWarnings("UnusedDeclaration")
public class SJIO {


    private static final Logger LOGGER = Logger.getLogger(
            Thread.currentThread().getStackTrace()[0].getClassName());
    private static boolean debugging;
    private static boolean debuggingOverRidden = false;
    private static boolean writeToLog;


    private static String logFileLocation;
    @Nullable
    private static BufferedWriter writer = null;
    /**
     * Default error message for int prompt
     */
    @SuppressWarnings("WeakerAccess")
    public final String INT_DEFAULT_ERROR = "Please input a valid integer";
    /**
     * Default error message for double prompt
     */
    @SuppressWarnings("WeakerAccess")
    public final String DBL_DEFAULT_ERROR = "Please input a valid number";
    /**
     * Default prompt
     */
    public final String DEFAULT_PROMPT = ">";
    private boolean logToFileOnly = false;
    private Scanner input = null;
    private PrintStream output = null;
    private boolean isNewLine = true;
    private boolean isAutoLog = false;

    /**
     * Constructor Takes inputsteam and initializes SJIO scanner to it.
     *
     * @param inputStream inputstream to read from
     * @param os          outputstream to write to
     */
    public SJIO(@NotNull InputStream inputStream, @NotNull OutputStream os) {
        input = new Scanner(inputStream);
        output = new PrintStream(os);
        checkDebugging();
        if (debugging) {
            log("net.kd8rho.util.SJIO", "init()", "Initializing SJIO with custom input parameter " + inputStream.toString() + " and output parameter " + os.toString(), LEVEL.INFO);
        }
    }

    /**
     * Constructor Takes inputsteam and initializes SJIO scanner to it.
     *
     * @param input Scanner to read from
     * @param os    outputstream to write to
     */
    public SJIO(@NotNull Scanner input, @NotNull OutputStream os) {
        this.input = input;
        output = new PrintStream(os);
        checkDebugging();
        if (debugging) {
            log("net.kd8rho.util.SJIO", "init()", "Initializing SJIO with custom input parameter " + input.toString() + " and output parameter " + os.toString(), LEVEL.INFO);
        }
    }

    /**
     * Constructor Takes inputsteam and initializes SJIO scanner to it.
     *
     * @param inputStream inputstream to read from
     */
    public SJIO(@NotNull InputStream inputStream) {
        input = new Scanner(inputStream);
        output = new PrintStream(System.out);
        checkDebugging();
        if (debugging) {
            log("net.kd8rho.util.SJIO", "init()", "Initializing SJIO with custom parameter " + inputStream.toString(), LEVEL.INFO);
        }
    }

    /**
     * Default Constructor Initializes scanner to System.in OK for most purposes
     */
    public SJIO() {
        if (output == null || input == null) {
            output = new PrintStream(System.out);
            input = new Scanner(System.in);
            checkDebugging();
            if (debugging) {
                log("net.kd8rho.util.SJIO", "public SJIO()", "Initializing SJIO with default parameter System.in", LEVEL.FINER);
            }
        } else {
            log("net.kd8rho.util.SJIO", "public SJIO()", "Already initialized", LEVEL.FINER);
        }
    }

    /**
     * Gets if the util is in DEBUG mode. DEBUG mode turns on logging and other functionality
     *
     * @return true if debug mode is on and false if debug mode is false
     */
    public static boolean isDebugging() {
        return debugging;
    }

    public static void setDebugging(boolean debugging1) {
        debugging = debugging1;
        debuggingOverRidden = true;
    }

    /**
     * Gets if autolog is on. Will write to file every time a log entry is made if enabled. May be bad for performance
     *
     * @return true if autolog is on
     */
    public boolean isAutoLog() {
        return isAutoLog;
    }

    /**
     * Sets autolog. Will write to file every time a log entry is made if enabled. May be bad for performance
     */
    public SJIO setAutoLog(boolean isAutoLog) {
        this.isAutoLog = isAutoLog;
        return this;
    }

    private SJIO checkDebugging() {
        if (!debuggingOverRidden && !System.getProperty("java.vendor.url").equals("http://www.android.com")) {
            try {
                debugging = java.lang.management.ManagementFactory.getRuntimeMXBean().
                        getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;
            } catch (NoClassDefFoundError e)//That class doesn't exist in Android!
            {
                debugging = false;
            }
        }
        return this;
    }

    /**
     * Returns if output to write to log file
     *
     * @return if output to write to log file
     */
    public boolean isWriteToLog() {
        return writeToLog;
    }

    /**
     * Sets output to write to log file, if set.
     *
     * @param writeToLog if output to write to log file
     */
    public SJIO setWriteToLog(boolean writeToLog) {
        SJIO.writeToLog = writeToLog;
        return this;
    }

    /**
     * Returns if the lib is set to only log to file
     *
     * @return boolean if set to log only to file
     */
    public boolean isLogToFileOnly() {
        return logToFileOnly;
    }

    /**
     * Sets whether to only log to file (Make sure to set a file to log to, or nothing will get logged!)
     *
     * @param logToFileOnly boolean if set to log only to file
     */
    public SJIO setLogToFileOnly(boolean logToFileOnly) {
        this.logToFileOnly = logToFileOnly;
        return this;
    }

    /**
     * Gets the location of the logfile. Can be null
     *
     * @return location of the logfile
     */
    @NotNull
    public String getLogFile() {
        return logFileLocation;
    }

    private boolean validateInt(int in, @NotNull ArrayList<numValidation> validations) {
        boolean good = false;
        for (numValidation validation : validations) {
            if (validation.validateInt(in))
                good = true;
            else {
                good = false;
                break;
            }
        }
        return good;
    }

    private boolean validateDouble(double in, @NotNull ArrayList<numValidation> validations) {
        boolean good = false;
        for (numValidation validation : validations) {
            if (validation.validateDouble(in))
                good = true;
            else {
                good = false;
                break;
            }
        }
        return good;
    }

    /**
     * sets a logfile to use in addition to the console. Send null to turn off file logging. Automatically appends to
     * existing file.
     *
     * @param theLogFile The log file to write to
     * @return whether setting the logfile was successful
     */
    public boolean setLogFile(@NotNull String theLogFile) {
        return setLogFile(theLogFile, true);
    }

    /**
     * Writes current log to logfile.
     *
     * @return true if success, false if failed.
     */
    public boolean writeLogFile() {
        try {
            if (writer != null) {
                writer.close();
                writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(logFileLocation, true), "utf-8"));
            } else
                return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * sets a logfile to use in addition to the console. Send null to turn off file logging
     *
     * @param theLogFile The log file to write to
     * @param append     whether or not to append to the existing log file
     * @return whether setting the logfile was successful
     */
    @SuppressWarnings({"SameParameterValue", "WeakerAccess"})
    public boolean setLogFile(@NotNull String theLogFile, boolean append) {
        logFileLocation = theLogFile;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(theLogFile, append), "utf-8"));
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    /**
     * Logs any error message from INFO to WARN
     *
     * @param theClass  The class the log is being called from
     * @param theMethod The method the log is being called from
     * @param message   Message to log
     * @param level     Log level
     */
    public void log(String theClass, String theMethod, String message, LEVEL level) {
        log(theClass, theMethod, message, level, false);
    }

    /**
     * Logs any error message from INFO to WARN
     *
     * @param theClass  The class the log is being called from
     * @param theMethod The method the log is being called from
     * @param message   Message to log
     * @param level     Log level
     * @param force     Whether to show even if debug mode off (irrelevant for WARN and ERROR messages)
     */
    @SuppressWarnings({"SameParameterValue", "WeakerAccess"})
    public void log(String theClass, String theMethod, String message, LEVEL level, boolean force) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String dtg = dateFormat.format(date);
        if (debugging || force) {
            if (level == LEVEL.INFO) {
                if (!logToFileOnly)
                    LOGGER.info(theClass + "." + theMethod + ": " + message);
                if (writer != null) {
                    try {
                        writer.write("[INFO]" + "[" + dtg + "]" + theClass + "." + theMethod + ": " + message + "\r\n");
                    } catch (IOException ignored) {
                    }
                }
            }
            if (level == LEVEL.FINE) {

                if (!logToFileOnly)
                    LOGGER.fine(theClass + "." + theMethod + ": " + message);
                if (writer != null) {
                    try {
                        writer.write("[FINE]" + "[" + dtg + "]" + theClass + "." + theMethod + ": " + message + "\r\n");
                    } catch (IOException ignored) {
                    }
                }
            }
            if (level == LEVEL.FINER) {
                if (!logToFileOnly)
                    LOGGER.finer(theClass + "." + theMethod + ": " + message);
                if (writer != null) {
                    try {
                        writer.write("[FINER]" + "[" + dtg + "]" + theClass + "." + theMethod + ": " + message + "\r\n");
                    } catch (IOException ignored) {
                    }
                }
            }
            if (level == LEVEL.WARN) {
                if (!logToFileOnly)
                    LOGGER.warning(theClass + "." + theMethod + ": " + message);
                if (writer != null) {
                    try {
                        writer.write("[WARN]" + "[" + dtg + "]" + theClass + "." + theMethod + ": " + message + "\r\n");
                    } catch (IOException ignored) {
                    }
                }
            }
            if (level == LEVEL.ERROR) {
                if (!logToFileOnly)
                    LOGGER.warning("E: " + theClass + "." + theMethod + ": " + message);
                if (writer != null) {
                    try {
                        writer.write("[ERROR]" + "[" + dtg + "]" + theClass + "." + theMethod + ": " + message + "\r\n");
                    } catch (IOException ignored) {
                    }
                }
            }
            try {
                if (writer != null && isAutoLog) {
                    writer.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if (level == LEVEL.WARN) {
                if (!logToFileOnly)
                    LOGGER.warning(theClass + "." + theMethod + ": " + message);
                if (writer != null) {
                    try {
                        writer.write("[WARN]" + "[" + dtg + "]" + theClass + "." + theMethod + ": " + message + "\r\n");
                        if (isAutoLog)
                            writer.flush();

                    } catch (IOException ignored) {
                    }
                }
            }
            if (level == LEVEL.ERROR) {
                if (!logToFileOnly)
                    LOGGER.warning("E: " + theClass + "." + theMethod + ": " + message);
                if (writer != null) {
                    try {
                        writer.write("[ERROR]" + "[" + dtg + "]" + theClass + "." + theMethod + ": " + message + "\r\n");
                        if (isAutoLog)
                            writer.flush();
                    } catch (IOException ignored) {
                    }
                }
            }
        }
    }

    /**
     * Logs any error message from INFO to WARN
     *
     * @param theClass  The class the log is being called from
     * @param theMethod The method the log is being called from
     * @param message   Message to log
     * @param level     Log level
     */
    @Deprecated
    public void log(String theClass, String theMethod, String message, String level) {
        //noinspection deprecation
        log(theClass, theMethod, message, level, false);
    }

    /**
     * Logs any error message from INFO to WARN
     *
     * @param theClass  The class the log is being called from
     * @param theMethod The method the log is being called from
     * @param message   Message to log
     * @param level     Log level
     * @param force     Whether to show even if debug mode off (irrelevant for WARN and ERROR messages)
     */
    @SuppressWarnings({"SameParameterValue", "WeakerAccess"})
    @Deprecated
    public void log(String theClass, String theMethod, String message, String level, boolean force) {
        if (debugging || force) {
            if (level.toLowerCase().equals("info")) {
                if (!logToFileOnly)
                    LOGGER.info(theClass + "." + theMethod + ": " + message);
            }
            if (level.toLowerCase().equals("fine")) {
                if (!logToFileOnly)
                    LOGGER.fine(theClass + "." + theMethod + ": " + message);
            }
            if (level.toLowerCase().equals("finer")) {
                if (!logToFileOnly)
                    LOGGER.finer(theClass + "." + theMethod + ": " + message);
            }
            if (level.toLowerCase().equals("warn")) {
                if (!logToFileOnly)
                    LOGGER.warning(theClass + "." + theMethod + ": " + message);
            }
            if (level.toLowerCase().equals("error")) {
                if (!logToFileOnly)
                    LOGGER.warning("E: " + theClass + "." + theMethod + ": " + message);
            }
        } else {
            if (level.toLowerCase().equals("warn")) {
                if (!logToFileOnly)
                    LOGGER.warning(theClass + "." + theMethod + ": " + message);
            }
            if (level.toLowerCase().equals("error")) {
                if (!logToFileOnly)
                    LOGGER.warning("E: " + theClass + "." + theMethod + ": " + message);
            }
        }
    }

    /**
     * Gets an int and performs error checking. No prompts or errors outputted
     *
     * @return valid integer response
     */
    public int getInt() {
        while (!input.hasNextInt()) {
            if (debugging)
                log("net.kd8rho.util.SJIO", "getInt()", "Bad int", LEVEL.WARN);
            input.next();
        }
        int in = input.nextInt();
        input.reset();
        return (in);
    }

    /**
     * Prompts for an int and returns only a valid int. Uses default non-int error message.
     *
     * @param message the prompt to display
     * @return a valid int
     */
    public int promptInt(String message) {
        write(message);
        while (!input.hasNextInt()) {
            writeLine(INT_DEFAULT_ERROR);
            write(message);
            if (debugging)
                log("net.kd8rho.util.SJIO", "promptInt()", "Bad int", LEVEL.WARN);
            input.next();
        }
        int in = input.nextInt();
        input.reset();
        return (in);
    }

    /**
     * Prompts for an int and returns only a valid int. Uses given non-int error message.
     *
     * @param message      the prompt to display
     * @param errorMessage error message for non-int
     * @return a valid int
     */
    public int promptInt(String message, String errorMessage) {
        write(message);
        while (!input.hasNextInt()) {
            writeLine(errorMessage);
            write(message);
            if (debugging)
                log("net.kd8rho.util.SJIO", "promptInt()", "Bad int", LEVEL.WARN);
            input.next();
        }
        int in = input.nextInt();
        input.reset();
        return (in);
    }

    /**
     * Prompts for an int and returns only a valid int between min and max. Uses given non-int error message.
     *
     * @param message      the prompt to display
     * @param errorMessage error message for non-int
     * @param min          the minimum value to accept
     * @param max          the maximum value to accept
     * @return a valid int between min and max
     */
    public int promptInt(String message, String errorMessage, int min, int max) {
        write(message);
        boolean good = false;
        int in = 0;
        while (!good) {
            while (!input.hasNextInt()) {
                writeLine(errorMessage);
                write(message);
                if (debugging)
                    log("net.kd8rho.util.SJIO", "promptInt()", "Bad int", LEVEL.WARN);
                input.next();
            }
            in = input.nextInt();
            if (in < min || in > max) {
                if (debugging)
                    log("net.kd8rho.util.SJIO", "promptInt()", "Input not between " + min + " and " + max, LEVEL.WARN);
                writeLine("Please input a number between " + min + " and " + max);
                writeLine(message);
            } else {
                good = true;
            }

        }
        input.reset();
        return (in);
    }

    /**
     * Prompts for an int and returns only a valid int between min and max. Uses given non-int error message.
     *
     * @param message      the prompt to display
     * @param errorMessage error message for non-int
     * @param format       A formatted input validation syntax.
     * @return a valid int between min and max
     */
    public int promptInt(String message, String errorMessage, String format) {
        ArrayList<String> formatgroups = new ArrayList<>(Arrays.asList(format.split("\\|")));
        ArrayList<ArrayList<numValidation>> validations = new ArrayList<>();
        for (String formatgroup : formatgroups) {
            ArrayList<String> formats = new ArrayList<>(Arrays.asList(formatgroup.split("&")));
            ArrayList<numValidation> thevalidations = new ArrayList<>();
            for (String format1 : formats) {
                thevalidations.add(new numValidation(format1));
            }
            validations.add(thevalidations);
        }
        write(message);
        boolean good = false;
        int in = 0;
        while (!good) {
            good = false;
            boolean validNum=false;
            while (!validNum) {
                try {
                    in = Integer.parseInt(input.nextLine());
                    validNum=true;
                }
                catch(NumberFormatException e)
                {
                    validNum=false;
                }
            }
            for (ArrayList<numValidation> validation : validations) {
                if (validateInt(in, validation)) {
                    good = true;
                    break;
                } else {
                    good = false;
                }
            }
            if (!good) {
                if (debugging)
                    log("net.kd8rho.util.SJIO", "promptInt()", "Failed validation", LEVEL.WARN);
                writeLine(errorMessage);
                write(message);
            }


        }
        return (in);
    }

    /**
     * Gets an int and performs error checking, ensuring it is between min and max. No prompts or errors outputted.
     *
     * @param min the minimum value to accept
     * @param max the maximum value to accept
     * @return a valid int between min and max
     */
    public int getInt(int min, int max) {
        boolean good = false;
        int in = 0;
        while (!good) {
            boolean validNum=false;
            while (!validNum) {
                try {
                    in = Integer.parseInt(input.nextLine());
                    validNum=true;
                }
                catch(NumberFormatException e)
                {
                    validNum=false;
                }
            }
            if (in < min || in > max) {
                if (debugging)
                    log("net.kd8rho.util.SJIO", "getInt()", "Input not between " + min + " and " + max, LEVEL.WARN);
            } else {
                good = true;
            }

        }
        return (in);
    }

    /**
     * Gets a double and performs error checking. No prompts or errors outputted.
     *
     * @return a valid double
     */
    public double getDouble() {
        while (!input.hasNextDouble()) {
            if (debugging)
                log("net.kd8rho.util.SJIO", "getDouble()", "Bad double", LEVEL.WARN);
            input.next();
        }
        double in = input.nextDouble();
        input.reset();
        return in;
    }

    /**
     * Prompts for an double and returns only a valid double. Uses default non-double error message.
     *
     * @param message the prompt to display
     * @return a valid double
     */
    public double promptDouble(String message) {
        write(message);
        while (!input.hasNextDouble()) {
            writeLine(DBL_DEFAULT_ERROR);
            write(message);
            if (debugging)
                log("net.kd8rho.util.SJIO", "promptDouble()", "Bad double", LEVEL.WARN);
            input.next();
        }
        double in = input.nextDouble();
        input.reset();
        return in;
    }

    /**
     * Prompts for an double and returns only a valid double. Uses given non-double error message.
     *
     * @param message      the prompt to display
     * @param errorMessage error message for non-double
     * @return a valid double
     */
    public double promptDouble(String message, String errorMessage) {
        write(message);
        while (!input.hasNextDouble()) {
            writeLine(errorMessage);
            write(message);
            if (debugging)
                log("net.kd8rho.util.SJIO", "promptDouble()", "Bad double", LEVEL.WARN);
            input.next();
        }
        double in = input.nextDouble();
        input.reset();
        return in;
    }

    /**
     * Prompts for an double and returns only a valid double. Uses given non-double error message.
     *
     * @param message      the prompt to display
     * @param errorMessage error message for non-double
     * @param format       the format to look for
     * @return a valid double
     */
    public double promptDouble(String message, String errorMessage, String format) {
        ArrayList<String> formatgroups = new ArrayList<>(Arrays.asList(format.split("\\|")));
        ArrayList<ArrayList<numValidation>> validations = new ArrayList<>();
        for (String formatgroup : formatgroups) {
            ArrayList<String> formats = new ArrayList<>(Arrays.asList(formatgroup.split("&")));
            ArrayList<numValidation> thevalidations = new ArrayList<>();
            for (String format1 : formats) {
                thevalidations.add(new numValidation(format1));
            }
            validations.add(thevalidations);
        }
        write(message);
        boolean good = false;
        double in = 0;
        while (!good) {
            good = false;
            while (!input.hasNextDouble()) {
                writeLine(errorMessage);
                write(message);
                if (debugging)
                    log("net.kd8rho.util.SJIO", "promptDouble()", "Bad double", LEVEL.WARN);
                input.next();
            }
            in = input.nextDouble();
            for (ArrayList<numValidation> validation : validations) {
                if (validateDouble(in, validation)) {
                    good = true;
                    break;
                } else {
                    good = false;
                }
            }
            if (!good) {
                if (debugging)
                    log("net.kd8rho.util.SJIO", "promptDouble()", "Failed validation", LEVEL.WARN);
                writeLine(errorMessage);
                write(message);
            }


        }
        input.reset();
        return (in);
    }

    /**
     * Gets a valid String. No prompts or error messages displayed.
     *
     * @return a valid String
     */
    public String getString() {
        return (input.next());
    }

    /**
     * Gets a valid String line. No prompts or error messages displayed.
     *
     * @return a valid String
     */
    public String getLine() {
        return (input.next());
    }

    /**
     * Gets a valid String. Displays a prompt
     *
     * @param message the prompt to display
     * @return a valid String
     */
    public String promptString(String message) {
        write(message);
        return (input.next());
    }

    /**
     * Gets a valid String. Displays a prompt
     *
     * @param message the prompt to display
     * @return a valid String
     */
    public String promptLine(String message) {
        write(message);
        return (input.nextLine());
    }

    /**
     * Writes a message, no newline
     *
     * @param message the message to write
     */
    public void write(String message) {
        if (writeLogFile()) {
            if (writer != null) {
                try {
                    if (isNewLine)
                        writer.write("[CONSOLE]:" + message.replaceAll("\u001B\\[[;\\d]*m", ""));
                    else
                        writer.write(message.replaceAll("\u001B\\[[;\\d]*m", ""));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        output.print(message);
        isNewLine = message.contains("\n");
    }

    /**
     * Writes a message with a newline following it
     *
     * @param message the message to write
     */
    public void writeLine(String message) {
        write(message + System.getProperty("line.separator"));
    }

    /**
     * Log levels
     */
    public enum LEVEL {
        INFO, FINE, FINER, WARN, ERROR
    }

    private class numValidation {
        @SuppressWarnings("CanBeFinal")
        private String format;
        @SuppressWarnings("CanBeFinal")
        private String number;

        public numValidation(String exp) {
            format = exp.substring(0, 1);
            number = exp.substring(1);
        }

        public boolean validateInt(int in) {
            switch (format) {
                case ">":
                    return (in > Integer.parseInt(number));
                case "<":
                    return (in < Integer.parseInt(number));
                case "=":
                    return (in == Integer.parseInt(number));
                case "!":
                    return (!(in == Integer.parseInt(number)));
            }
            return false;
        }

        public boolean validateDouble(double in) {
            switch (format) {
                case ">":
                    return (in > Double.parseDouble(number));
                case "<":
                    return (in < Double.parseDouble(number));
                case "=":
                    return (in == Double.parseDouble(number));
                case "!":
                    return (!(in == Double.parseDouble(number)));
            }
            return false;
        }
    }

}
