/*
 * Copyright 2014 Steven Jennison https://kd8rho.net, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * POJO to store names.
 *
 * Created by Steven on 6/25/2014. Package: net.kd8rho.util.data Project: KD8RHO Utility Library
 */
@SuppressWarnings("UnusedDeclaration")
public class Name {
    @Nullable
    private String prefix;
    @Nullable
    private String suffix;
    @Nullable
    private String first;
    @Nullable
    private String middle;
    @Nullable
    private String last;

    /**
     * Constructor with first and last
     * @param first first name
     * @param last last name
     */
    public Name(@Nullable String first, @Nullable String last) {
        this.first = first;
        this.last = last;
    }

    /**
     * Constructor with first, last, and middle name
     * @param first first name
     * @param middle middle name
     * @param last last name
     */
    public Name(@Nullable String first, @Nullable String middle, @Nullable String last) {

        this.first = first;
        this.middle = middle;
        this.last = last;
    }


    /**
     * Constructor with prefix, first name, middle name, last name, and suffix
     * @param prefix prefix to name (Mr, Mrs, etc)
     * @param first first name
     * @param middle middle name
     * @param last last name
     * @param suffix suffix to name
     */
    public Name(@Nullable String prefix, @Nullable String first, @Nullable String middle, @Nullable String last, @Nullable String suffix) {

        this.prefix = prefix;
        this.suffix = suffix;
        this.first = first;
        this.middle = middle;
        this.last = last;
    }

    /**
     * Default constructor to initialize empty Name
     */
    public Name() {

    }

    /**
     * Attempts to parse a given string into a first, last, and possibly MI
     * @param name the string to parse
     * @param secondIsMiddle second name is a middle name, not part of first
     * @return the parsed Name object
     */
    public static Name parseName(@NotNull String name, boolean secondIsMiddle) {
        Name theName = new Name();
        String[] splitString = name.split(" ");
        theName.setFirst(splitString[0]);
        if (splitString[1].contains("\\.")) {
            theName.setMiddle(splitString[1]);//If the second string has a . in it, it's probably a middle initial
        } else if (!secondIsMiddle) {
            theName.setFirst(splitString[0] + " " + splitString[1]);
        }
        theName.setLast(splitString[2]);
        return theName;
    }

    /**
     * Gets the prefix
     * @return the prefix
     */
    @Nullable
    public String getPrefix() {
        return prefix;
    }

    /**
     * Sets the prefix
     * @param prefix the prefix
     */
    public void setPrefix(@Nullable String prefix) {
        this.prefix = prefix;
    }

    /**
     * Gets the suffix
     * @return the suffix
     */
    @Nullable
    public String getSuffix() {
        return suffix;
    }

    /**
     * Sets the suffix
     * @param suffix the suffix
     */
    public void setSuffix(@Nullable String suffix) {
        this.suffix = suffix;
    }

    /**
     * Gets the first name
     * @return the first name
     */
    @Nullable
    public String getFirst() {
        return first;
    }

    /**
     * Sets the first name
     * @param first the first name
     */
    public void setFirst(@Nullable String first) {
        this.first = first;
    }

    /**
     * Gets the middle name/middle initial
     * @return the middle name/initial
     */
    @Nullable
    public String getMiddle() {
        return middle;
    }

    /**
     * Sets the middle name/middle initial
     * @param middle the middle name/initial
     */
    public void setMiddle(@Nullable String middle) {
        this.middle = middle;
    }

    /**
     * Gets the last name
     * @return the last name
     */
    @Nullable
    public String getLast() {
        return last;
    }

    /**
     * Sets the last name
     * @param last the last name
     */
    public void setLast(@Nullable String last) {
        this.last = last;
    }

    /**
     * Returns a string of the name, formatted.
     * Format:<br/>
     * Variables:<br/>
     * %P% - Prefix<br/>
     * %F% - First name<br/>
     * %M% - Middle name<br/>
     * %MI% - Middle initial<br/>
     * %L% - Last name<br/>
     * %S% - Suffix<br/>
     * @param inputFormat the string with variables to replace with the name
     * @return formatted name
     */
    @NotNull
    public String toFormattedString(@NotNull String inputFormat) {
        String PREFIX = "%P%", FIRST = "%F%", MIDDLE = "%M%", MIDDLE_INITIAL = "%MI%", LAST = "%L%", SUFFIX = "%S%";
        String prefix1 = "", first1 = "", middle1 = "", last1 = "", suffix1 = "";
        if (prefix != null) {
            prefix1 = prefix;
        }
        if (first != null) {
            first1 = first;
        }
        if (middle != null) {
            middle1 = middle;
        }
        if (last != null) {
            last1 = last;
        }
        if (suffix != null)
        {
            suffix1 = suffix;
        }
        return inputFormat.replaceAll(PREFIX, prefix1).replaceAll(FIRST, first1).replaceAll(MIDDLE, middle1).replaceAll(MIDDLE_INITIAL, middle1.length() > 1 ? String.valueOf(middle1.charAt(0)) : "").replaceAll(LAST, last1).replaceAll(SUFFIX, suffix1);
    }

    /**
     * Returns the full name, with prefix first middle last suffix
     * @return full name
     */
    @NotNull
    @Override
    public String toString() {
        String out = "";
        if (prefix != null) {
            out += out + prefix + " ";
        }
        if (first != null) {
            out += out + first + " ";
        }
        if (middle != null) {
            out += out + middle + " ";
        }
        if (last != null) {
            out += out + last + " ";
        }
        if (suffix != null)
        {
            out += out + suffix + " ";
        }
        return out;
    }
}
