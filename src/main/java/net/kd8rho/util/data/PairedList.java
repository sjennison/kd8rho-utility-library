/*
 * Copyright 2014 Steven Jennison https://kd8rho.net, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;

/**
 * Created by Steven on 8/14/2014. Package: net.kd8rho.util.data Project: Util
 * A paired list that contains two lists of objects. Each list must have the same size.
 * This allows us to compare objects from either list
 */
public class PairedList<FirstObject, SecondObject> {
    @NotNull
    public ArrayList<FirstObject> firstList;
    @NotNull
    public ArrayList<SecondObject> secondList;

    /**
     * Constructor that sets the lists. They must be the same size
     * @param firstList an ArrayList of objects
     * @param secondList an ArrayList of objects
     * @exception java.util.InputMismatchException if the two ArrayLists are not the same size
     */
    public PairedList(@NotNull ArrayList<FirstObject> firstList, @NotNull ArrayList<SecondObject> secondList) {
        if(firstList.size()!=secondList.size())
        {
            throw new InputMismatchException("Lengths of ArrayLists must be the same!");
        }
        this.firstList = firstList;
        this.secondList = secondList;
    }

    /**
     * Constructor that sets the lists. They must be the same size
     * @param firstList an array of objects
     * @param secondList an array of objects
     * @exception java.util.InputMismatchException if the two ArrayLists are not the same size
     */
    public PairedList(@NotNull FirstObject[] firstList, @NotNull SecondObject[] secondList) {
        if(firstList.length!=secondList.length)
        {
            throw new InputMismatchException("Lengths of ArrayLists must be the same!");
        }
        this.firstList = new ArrayList<>();
        this.firstList.addAll(Arrays.asList(firstList));
        this.secondList = new ArrayList<>();
        this.secondList.addAll(Arrays.asList(secondList));
    }

    /**
     * Gets the first list.
     * @return the first list
     */
    @NotNull
    public ArrayList<FirstObject> getFirstList() {
        ArrayList<FirstObject> returnList=new ArrayList<>();
        for (FirstObject firstObject : firstList) {
            returnList.add(firstObject);
        }
        return returnList;
    }

    /**
     * Gets the second list
     * @return the second list
     */
    @NotNull
    public ArrayList<SecondObject> getSecondList() {
        ArrayList<SecondObject> returnList=new ArrayList<>();
        for (SecondObject secondObject : secondList) {
            returnList.add(secondObject);
        }
        return returnList;
    }

    /**
     * Sets the lists. They must be the same size
     * @param firstList an ArrayList of objects
     * @param secondList an ArrayList of objects
     * @exception java.util.InputMismatchException if the two ArrayLists are not the same size
     */
    public void setLists(@NotNull ArrayList<FirstObject> firstList, @NotNull ArrayList<SecondObject> secondList)
    {
        if(firstList.size()!=secondList.size())
        {
            throw new InputMismatchException("Lengths of ArrayLists must be the same!");
        }
        this.firstList=firstList;
        this.secondList=secondList;
    }

    /**
     * Sets the lists. They must be the same size
     * @param firstList an array of objects
     * @param secondList an array of objects
     * @exception java.util.InputMismatchException if the two ArrayLists are not the same size
     */
    public void setLists(@NotNull FirstObject[] firstList, @NotNull SecondObject[] secondList)
    {
        if(firstList.length!=secondList.length)
        {
            throw new InputMismatchException("Lengths of ArrayLists must be the same!");
        }
        this.firstList = (ArrayList<FirstObject>) Arrays.asList(firstList);
        this.secondList= (ArrayList<SecondObject>) Arrays.asList(secondList);
    }

    /**
     * Adds an object to both lists at the same index
     * @param firstObject the object to add to the first list
     * @param secondObject the object to add to the second list
     */
    public void add(FirstObject firstObject,SecondObject secondObject)
    {
        firstList.add(firstObject);
        secondList.add(secondObject);
    }

    /**
     * Adds an object to both lists at the same index
     * @param firstObject the object to add to the first list
     * @param secondObject the object to add to the second list
     * @param index the index to add to
     */
    public void add(FirstObject firstObject,SecondObject secondObject, int index)
    {
        firstList.add(index,firstObject);
        secondList.add(index,secondObject);
    }

    /**
     * Returns an object from the first list from the same index in the second list as the given object
     * @param object the object to search for
     * @return an object from the first list matching the second one. Null if not found.
     */
    @Nullable
    public FirstObject getFirstListMatchSecond(@NotNull SecondObject object)
    {
        for (int i = 0; i < secondList.size(); i++) {
            if(object.equals(secondList.get(i)))
            {
                return firstList.get(i);
            }
        }
        return null;
    }

    /**
     * Returns an object from the second list from the same index in the first list as the given object
     * @param object the object to search for
     * @return an object from the second list matching the first one. Null if not found
     */
    @Nullable
    public SecondObject getSecondListMatchFirst(@NotNull FirstObject object)
    {
        for (int i = 0; i < firstList.size(); i++) {
            if(object.equals(firstList.get(i)))
            {
                return secondList.get(i);
            }
        }
        return null;
    }
}
