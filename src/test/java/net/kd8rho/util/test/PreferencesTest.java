/*
 * Copyright 2014 Steven Jennison https://kd8rho.net, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util.test;

import junit.framework.TestCase;
import net.kd8rho.util.Preferences;
import net.kd8rho.util.SJFIO;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import java.io.File;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PreferencesTest extends TestCase {

    private static final String FILE_NAME="test.preferences.conf";

    private Preferences theTestPreferences;
    public void setUp() throws Exception {
        super.setUp();

        theTestPreferences=new Preferences();
        theTestPreferences.setSettingFileLocation(FILE_NAME);

        SJFIO thePrefFile=new SJFIO(FILE_NAME);
        thePrefFile.theFileInMem.add("test_preferences: test1");
        thePrefFile.theFileInMem.add("test_preferences_2: test2");
        thePrefFile.theFileInMem.add("#this is a comment");
        thePrefFile.write();
    }

    public void testexecuteaLoadFromFile() throws Exception {
        System.out.println("Load from file");
        theTestPreferences.loadFromFile(FILE_NAME);
    }


    @SuppressWarnings("ConstantConditions")//I don't care if this test blows up with a null pointer exception, that's what it's there for
    public void testexecutebGetPreference() throws Exception {
        System.out.println("Get prefs");
        createResult();
        theTestPreferences.loadFromFile(FILE_NAME);
        assertEquals(theTestPreferences.getPreference("test_preferences").getValue(),"test1");
        assertEquals(theTestPreferences.getPreference("test_preferences_2").getValue(),"test2");
        assertNull(theTestPreferences.getPreference("#this is a comment"));//comments should be null
    }


    @SuppressWarnings("ConstantConditions")//Still don't care...
    public void testexecutecSetPreference() throws Exception {
        System.out.println("Set prefs");
        theTestPreferences.loadFromFile(FILE_NAME);
        theTestPreferences.setPreference("test_preferences_3","test3");//Set a third one to test...
        assertEquals(theTestPreferences.getPreference("test_preferences_3").getValue(),"test3");
    }

    public void testexecutedSaveToFile() throws Exception {
        System.out.println("Save to file");
        theTestPreferences.loadFromFile(FILE_NAME);
        theTestPreferences.setPreference("test_preferences_3","test3");//Set a third one to test...
        theTestPreferences.saveToFile();
        SJFIO thePrefFile=new SJFIO(FILE_NAME);
        thePrefFile.read();
        assertEquals(thePrefFile.theFileInMem.get(0), "test_preferences: test1");
        assertEquals(thePrefFile.theFileInMem.get(1),"test_preferences_2: test2");
        assertEquals(thePrefFile.theFileInMem.get(2),"#this is a comment");
        assertEquals(thePrefFile.theFileInMem.get(3),"test_preferences_3: test3");
    }

    public void tearDown() throws Exception {
        boolean delete = new File(FILE_NAME).delete();
        if(!delete)
            System.err.println("Failed to delete test file!");
    }
}