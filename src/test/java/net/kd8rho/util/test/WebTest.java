/*
 * Copyright 2014 Steven Jennison https://kd8rho.net, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util.test;

import junit.framework.TestCase;
import net.kd8rho.util.SJIO;
import net.kd8rho.util.Web;

import java.util.ArrayList;

public class WebTest extends TestCase {

    private static SJIO io = new SJIO();

    public void testGetURLTextToArray() throws Exception {
        io.write("Getting test file...");
        ArrayList<String> theResult = Web.getURLTextToArray("http://www.msftncsi.com/ncsi.txt");
        io.writeLine("[OK]");

        io.write("Testing if result is null...");
        //Check to see if result is null, fail if so
        assertNotNull(theResult);
        io.writeLine("[OK]");

        io.write("Testing result length...");
        //Check length, should be 1
        assertEquals(1, theResult.size());
        io.writeLine("[OK]");

        io.write("Testing content...");
        //Check content
        assertEquals("Microsoft NCSI", theResult.get(0));
        io.writeLine("[OK]");

        //Pass!

    }
}