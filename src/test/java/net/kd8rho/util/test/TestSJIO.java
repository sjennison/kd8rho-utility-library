/*
 * Copyright 2014 Steven Jennison https://kd8rho.net, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kd8rho.util.test;

import junit.framework.TestCase;
import net.kd8rho.util.SJIO;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by Steven on 5/28/14. Package: net.kd8rho.util Project: Util
 */
public class TestSJIO extends TestCase {
    private static SJIO io=new SJIO();
    public void setUp() throws Exception {
        super.setUp();
        io.setLogFile("UnitTestLogFile");

    }

    public void testPromptInt() throws Exception {
        System.out.print("Testing SJIO.promptInt()......................");
        String theTestData="a\r\n1.4\r\n3\r\n";//Test data. First one contains invalid characters. Second is a double. Third is valid
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayInputStream inContent=new ByteArrayInputStream(theTestData.getBytes());
        io=new SJIO(inContent,outContent);
        int response=io.promptInt("Must get number");
        assertTrue(response!=1.4);
        assertTrue(response==3);
        System.out.println("[OK]");

    }

    public void testPromptIntValidation() throws Exception {
        System.out.print("Testing SJIO.promptInt() with validation......");
        String theTestData="a\r\n1.4\r\n3\r\n15\r\n";//Test data. First one contains invalid characters. Second is a double. Third is valid, but not in the correct range. Fourth is correct.
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayInputStream inContent=new ByteArrayInputStream(theTestData.getBytes());
        io=new SJIO(inContent,outContent);
        int response=io.promptInt("Must get number","Please input a valid number",">10");
        assertTrue(response!=1.4);
        assertTrue(response!=3);
        assertEquals(response,15);
        System.out.println("[OK]");
    }

    public void testPromptDouble() throws Exception {
        System.out.print("Testing SJIO.promptDouble()...................");
        String theTestData="a\r\n1.4\r\n";//Test data. First one contains invalid characters. Second is valid
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayInputStream inContent=new ByteArrayInputStream(theTestData.getBytes());
        io=new SJIO(inContent,outContent);
        double response=io.promptDouble("Must get number");
        assertTrue(response==1.4d);
        System.out.println("[OK]");
    }

    public void testPromptDoubleValidation() throws Exception {
        System.out.print("Testing SJIO.promptDouble() with validation...");
        String theTestData="a\r\n1.4\r\n16.5\r\n";//Test data. First one contains invalid characters. Second is valid, but not in the range. Third is correct
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayInputStream inContent=new ByteArrayInputStream(theTestData.getBytes());
        io=new SJIO(inContent,outContent);
        double response=io.promptDouble("Must get number","Please input a valid number",">10");
        assertTrue(response!=1.4d);
        assertEquals(response,16.5d);
        System.out.println("[OK]");
    }

    public void testPromptLine() throws Exception {
        System.out.print("Testing SJIO.promptLine().....................");
        String theTestData="Hello World\r\nThis data is irrelevant\r\n";//Test data.
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayInputStream inContent=new ByteArrayInputStream(theTestData.getBytes());
        io=new SJIO(inContent,outContent);
        String response=io.promptLine("Must get full string");
        assertEquals(response,"Hello World");
        System.out.println("[OK]");
    }

    public void testWriteLine() throws Exception {
        System.out.print("Testing SJIO.writeLine()......................");
        String theTestData="This data is irrelevant\r\n";//Test data.
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayInputStream inContent=new ByteArrayInputStream(theTestData.getBytes());
        io=new SJIO(inContent,outContent);
        io.writeLine("Test Message");
        assertEquals(outContent.toString(), "Test Message" + System.getProperty("line.separator"));
        System.out.println("[OK]");
    }

    public void testWrite() throws Exception {
        System.out.print("Testing SJIO.write()..........................");
        String theTestData = "This data is irrelevant\r\n";//Test data.
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayInputStream inContent = new ByteArrayInputStream(theTestData.getBytes());
        io = new SJIO(inContent, outContent);
        io.write("Test Message");
        assertEquals(outContent.toString(), "Test Message");
        System.out.println("[OK]");
    }
}
